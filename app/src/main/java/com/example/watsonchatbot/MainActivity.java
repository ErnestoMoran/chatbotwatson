package com.example.watsonchatbot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    TextView messagesTextView;
    EditText inputEditText;
    Button sendButton;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        messagesTextView = findViewById(R.id.messagesTextView);
        inputEditText = findViewById(R.id.inputEditText);
        sendButton = findViewById(R.id.sendButton);
        context = this;
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = inputEditText.getText().toString();
                messagesTextView.append(Html.fromHtml("<p><b>Tu:</b>"+ input +"</p>"));
                inputEditText.setText("");

                getResponse(input,"");
            }
        });

    }

    private void getResponse(String input,String contexto) {
        String workspaceId="097aaacd-203a-4326-9f4d-a66ef57bd32f";
        String skillId="9e4af99c-c5ef-4ccf-b305-3b02cdff47df";
        String assitantsId="9083363f-94cb-4ab9-a179-c07fe9afb3f0";
        String sessionsId="";
        String urlAssistantV2="";
        String urlAssistant="https://api.us-south.assistant.watson.cloud.ibm.com/instances/"+workspaceId+"/v1/workspaces/"+skillId+"/message?version=2020-02-05";
        String authentication="YXBpS2V5OjJYeHljUmNrQzJ3UVpPN2NzTXU3b1lTcGZFTllYNzFOR1drekJUNUgxX1NW";

        sessionsId=getSession(workspaceId,assitantsId);

        JSONObject inputJsonObject=new JSONObject();
        try {
            inputJsonObject.put("text",input );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject contextoJson = new JSONObject();
        try {
            contextoJson.put("titulo",contexto);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("input",inputJsonObject);
            jsonBody.put("context",contextoJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post(urlAssistant)
                .addHeaders("Content-Type","application/json")
                .addHeaders("Authorization","Basic " + authentication)
                .addJSONObjectBody(jsonBody)
                .setPriority(Priority.HIGH)
                .setTag(getString(R.string.app_name))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String outputAssistant = "";
                        try {
                            if(response.has("actions")){
                                Integer outputJsonObject2 = response.getJSONArray("actions").getJSONObject(0).getJSONObject("parameters").getInt("id");
                                messagesTextView.append(Html.fromHtml("<p><b>Chatbot:</b>"+ outputJsonObject2 +"</p>"));
                                getExterno(outputJsonObject2);
                            }else{
                                String outputJsonObject = response.getJSONObject("output").getJSONArray("text").getString(0);
                                messagesTextView.append(Html.fromHtml("<p><b>Chatbot:</b>"+ outputJsonObject +"</p>"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(context,"Error de conexión",Toast.LENGTH_LONG).show();
                    }
                });

    }
    private void getExterno(int id){
        String urlExterno="https://swapi.co/api/people/"+id;


        AndroidNetworking.post(urlExterno)
                .addHeaders("Content-Type","application/json")
                .setPriority(Priority.HIGH)
                .setTag(getString(R.string.app_name))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String outPutExterno=response.getString("name");
                            messagesTextView.append(Html.fromHtml("<p><b>Chatbot:</b>"+ outPutExterno +"</p>"));
                            getResponse(outPutExterno,outPutExterno);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
    public String getSession(String workspaceId,String assistantId){
        String urlSession="https://api.us-south.assistant.watson.cloud.ibm.com/instances/"+workspaceId+ "/v2/assistants/"+assistantId+"/message?version=2020-02-05";
        String authentication="YXBpS2V5OjJYeHljUmNrQzJ3UVpPN2NzTXU3b1lTcGZFTllYNzFOR1drekJUNUgxX1NW";
        final String[] sessions = new String[1];
        AndroidNetworking.post(urlSession)
                .addHeaders("Content-Type","application/json")
                .addHeaders("Authorization","Basic " + authentication)
                .setPriority(Priority.HIGH)
                .setTag(getString(R.string.app_name))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String outPutSession=response.getString("session_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        return "";
    }
}
