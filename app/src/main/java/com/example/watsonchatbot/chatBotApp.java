package com.example.watsonchatbot;

import android.app.Application;
import android.util.AndroidRuntimeException;

import com.androidnetworking.AndroidNetworking;

public class chatBotApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
    }
}
